<?php

/**
 * @file
 * Hooks related to Ckeditor5 Entity Browser module.
 */

/**
 * Alter the config provided in getDynamicPluginConfig.
 *
 * @param array $definitions
 *   The array of dynamic entity browser config.
 */
function hook_ckeditor5_entity_browser_definitions_alter(array &$definitions) {
  foreach ($definitions as $entity_browser_id => $config) {
    if ('eur_content_browser' === $entity_browser_id) {
      $definitions[$entity_browser_id] = [
        'widget_context' => [
          // Your stuff...
        ],
        'label' => t('Custom label'),
        'weight' => 1,
      ];
    }
  }
}
