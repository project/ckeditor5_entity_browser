# Ckeditor 5 entity browser

## Overview
This module integrates entity browsers with Ckeditor's link UI.

## Features
* Provide buttons in link UI which open entity browsers
* Insert selected entity in the link UI
* Configure desired entity browsers per text format
* Customize settings using a hook alter
* Works together with other link UI plugins like linkit.

## Installation
* Enable the module
* Configure at least one entity browser
* Make sure the view has a bulk select form
* Navigate to a text format which has ckeditor5 enabled
* Enable at least one entity browser

Note: it is recommended to enable 'Use field cardinality'
in the entity browser view. This will show radio buttons
instead of checkboxes which makes sense, since you always
insert one link at a time.

## Dev notes
Currently, you can use the provided hook alter to change any
config you need. For example button weights and labels.

## Limitations
* Only canonical entities can be inserted.
