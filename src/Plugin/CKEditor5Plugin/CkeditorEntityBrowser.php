<?php

declare(strict_types=1);

namespace Drupal\ckeditor5_entity_browser\Plugin\CKEditor5Plugin;

use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableInterface;
use Drupal\ckeditor5\Plugin\CKEditor5PluginConfigurableTrait;
use Drupal\ckeditor5\Plugin\CKEditor5PluginDefault;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\EditorInterface;
use Drupal\entity_browser\DisplayRouterInterface;
use Drupal\entity_browser\EntityBrowserInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * CKEditor 5 Entity Browser plugin configuration.
 */
class CkeditorEntityBrowser extends CKEditor5PluginDefault implements CKEditor5PluginConfigurableInterface {

  use CKEditor5PluginConfigurableTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'entity_browser_enabled' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   *
   * Form for choosing which entity browsers are available.
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['enabled_entity_browsers'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Enabled entity browsers'),
      '#description' => $this->t('These are the entity browsers that will be available in the link UI of CKeditor.'),
    ];

    $entity_browsers = \Drupal::entityTypeManager()->getStorage('entity_browser')->loadMultiple();
    /** @var \Drupal\entity_browser\EntityBrowserInterface[] $entity_browsers */
    foreach ($entity_browsers as $entity_browser) {
      $form['enabled_entity_browsers'][$entity_browser->id()] = [
        '#type' => 'checkbox',
        '#title' => $entity_browser->label(),
        '#return_value' => $entity_browser->id(),
      ];
      $form['enabled_entity_browsers'][$entity_browser->id()]['#default_value'] = in_array($entity_browser->id(), $this->configuration['enabled_entity_browsers'] ?? [], TRUE) ? $entity_browser->id() : NULL;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Match the config schema structure at ckeditor5.plugin.ckeditor5_heading.
    $form_value = $form_state->getValue('enabled_entity_browsers');
    $config_value = array_values(array_filter($form_value));
    $form_state->setValue('enabled_entity_browsers', $config_value);
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $this->configuration['entity_browser_enabled'] = !!$form_state->getValues()['enabled_entity_browsers'];
    if ($this->configuration['entity_browser_enabled']) {
      $this->configuration['enabled_entity_browsers'] = $form_state->getValue('enabled_entity_browsers');
    }
  }

  /**
   * Config validation callback.
   *
   * @param array $values
   *   The configuration subtree.
   * @param \Symfony\Component\Validator\Context\ExecutionContextInterface $context
   *   The validation execution context.
   *
   * @see ckeditor5_entity_browser.schema.yml
   */
  public static function requireEntityBrowserIfEnabled(array $values, ExecutionContextInterface $context): void {
    if (TRUE === $values['entity_browser_enabled'] && empty($values['enabled_entity_browsers'])) {
      $context->buildViolation(t('Entity browser is enabled, please select at least one entity browser.'))
        ->atPath('enabled_entity_browsers')
        ->addViolation();
    }
    elseif (FALSE === $values['entity_browser_enabled'] && !empty($values['enabled_entity_browsers'])) {
      $context->buildViolation(t('Entity browser is disabled; it does not make sense to associate an entity browser.'))
        ->atPath('enabled_entity_browsers')
        ->addViolation();
    }
  }

  /**
   * Computes all valid choices for the "entity_browser_enabled" setting.
   *
   * @see ckeditor5_entity_browser.schema.yml
   *
   * @return string[]
   *   All valid choices.
   */
  public static function validChoices(): array {
    $entity_browser_storage = \Drupal::service('entity_type.manager')->getStorage('entity_browser');
    assert($entity_browser_storage instanceof EntityStorageInterface);
    return array_keys($entity_browser_storage->loadMultiple());
  }

  /**
   * {@inheritdoc}
   */
  public function getDynamicPluginConfig(array $static_plugin_config, EditorInterface $editor): array {
    $enabled_entity_browsers = $this->configuration['enabled_entity_browsers'] ?? NULL;

    if (!$enabled_entity_browsers) {
      return $static_plugin_config;
    }

    $entity_browser_storage = \Drupal::service('entity_type.manager')->getStorage('entity_browser');
    $config = [];

    $w = 1;
    foreach ($enabled_entity_browsers as $entity_browser) {
      $entity_browser_entity = $entity_browser_storage->load($entity_browser);
      if (
        !$entity_browser_entity instanceof EntityBrowserInterface
        || !$entity_browser_entity->getDisplay() instanceof DisplayRouterInterface
      ) {
        continue;
      }
      $config[$entity_browser] = [
        'widget_context' => [
          'cardinality' => 1,
        ],
        'label' => $this->t('Select content'),
        'weight' => $w++,
        'browser_display_url' => $entity_browser_entity->getDisplay()->path(),
      ];
    }

    // Allow modules to alter the config.
    \Drupal::moduleHandler()->alter('ckeditor5_entity_browser_definitions', $config);

    foreach ($enabled_entity_browsers as $entity_browser) {
      $uuid = \Drupal::service('uuid')->generate();
      $config[$entity_browser]['uuid'] = $uuid;
      // We use this flag in the form alter.
      $config[$entity_browser]['ckeditor'] = TRUE;
      \Drupal::service('entity_browser.selection_storage')->setWithExpire(
        $uuid,
        $config[$entity_browser],
        21600
      );
    }

    return [
      'entity_browsers' => $config,
    ];
  }

}
