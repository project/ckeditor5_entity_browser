import { Plugin } from 'ckeditor5/src/core';
import { ButtonView, View } from 'ckeditor5/src/ui';

class LinkUIEntityBrowser extends Plugin {

  /**
   * @inheritdoc
   */
  static get pluginName() {
    return 'LinkUIEntityBrowser';
  }

  init() {
    const linkUI = this.editor.plugins.get('LinkUI');
    const _createFormViewOriginal = linkUI._createFormView;
    const _hideUIOriginal = linkUI._hideUI;

    const enabledEntityBrowsers = this.editor.config.get('entity_browsers');

    linkUI._createFormView = () => {
      const formView = _createFormViewOriginal.bind(linkUI).call();

      formView.extendTemplate({
        attributes: {
          class: ['ck-vertical-form', 'ck-link-form_layout-vertical'],
        },
      });

      let buttons = [];
      if (enabledEntityBrowsers) {
        const sorted = Object.entries(enabledEntityBrowsers).sort((a, b) => {
          return a[1].weight - b[1].weight;
        });
        for (const [, config] of sorted) {
          const button = new ButtonView();
          button.set({
            label: config.label,
            withText: true,
          });
          this.listenTo(button, 'execute', () => {
            // Note: while automatic placement in the button pane ui is nice, it's a bit
            // problematic when we have views with exposed filters which have their own
            // buttons. We do not want those to be placed in the button pane, this is why
            // drupalAutoButtons is set to false, but below core patch is needed.
            // @see https://www.drupal.org/project/drupal/issues/2793343
            // @todo see if we can exclude the exposed filter button because the button
            //   pane UX is really nice, especially on smaller screens.
            Drupal.ckeditor5.openDialog(
              `${config.browser_display_url}?uuid=${config.uuid}`,
              ( value ) => {
                linkUI.formView.urlInputView.fieldView.element.value = value;
                setTimeout(() => {
                  linkUI.formView.urlInputView.fieldView.element.focus();
                }, 100)
              },
              {
                drupalAutoButtons: false,
                dialogClass: 'ckeditor5-entity-browser-dialog',
              },
            );
          });
          buttons.push(button);
        }
      }

      const additionalButtonsView = new View();
      additionalButtonsView.setTemplate({
        tag: 'div',
        attributes: {
          class: ['ck', 'ck-reset'],
          style: {
            display: 'flex',
            alignItems: 'stretch',
            justifyContent: 'center',
            padding: 'var(--ck-spacing-small)',
            borderBottom: '1px solid var(--ck-color-base-border)'
          },
        },
        children: buttons
      });

      formView.template.children.unshift(additionalButtonsView);

      return formView;
    }

    linkUI._hideUI = () => {
      const modal = document.querySelectorAll('.ui-dialog-content');
      if (modal.length === 0) {
        _hideUIOriginal.bind(linkUI).call();
      }
    };
  }
}

export default {
  LinkUIEntityBrowser,
};
